const express = require('express');
const app = express();
const port = 3018;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const usuarios = require('./routes/usuarios');
const productos = require('./routes/productos');
const pedidos = require('./routes/pedidos');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API de restaurante',
      version: '1.0.0'
    }
  },
  apis: ['./routes/usuarios.js','./routes/productos.js','./routes/pedidos.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//rutas
app.use('/usuarios', usuarios);

app.use('/productos', productos);

app.use('/pedidos', pedidos);


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});