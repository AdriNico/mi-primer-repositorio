let varUser = require('../models/usuarios');

function validar_indice (req, res, next){
    if(req.body.indice >= 0 && req.body.indice < usuarios.length){
      console.log("el indice si existe")
        next();
    }
    else{
        return res.status(500).json({"indice": "invalido"});
    }
}


function validar_admin(req, res, next){
   // let username = req.body.username;

    console.log(varUser.usuario_logueado.id)
    if(varUser.usuario_logueado.id !==undefined){ 
        if(varUser.usuario_logueado.type !== "admin"){
            res.json('no tienes permiso para modificar el producto')
        }else{
            next();
        }
    }else{
        res.json('no estas loggeado');
    }
};

function validar_cliente(req, res, next){
    // let username = req.body.username;

    console.log(varUser.usuario_logueado.id)
    if(varUser.usuario_logueado.id !==undefined){ 
        if(varUser.usuario_logueado.id ==undefined ){
            res.json('no eres cliente')
        }else{
            next();
        }
    }else{
        res.json('Por favor logeate primero');
    }
};

module.exports ={ validar_indice, validar_admin, validar_cliente};





