const express = require('express');
const router = express.Router();
let pedidos = require('../models/pedidos');

/**
 * @swagger
 * /pedidos/:
 *  get:
 *    description: lista todos los pedidos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
    res.json(pedidos);
});

/**
 * @swagger
 * /pedidos:
 *  post:
 *    description: crea un pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: pedidos
 *      description: pedidos del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    - name: estado
 *      description: estado del pedido
 *      in: formData
 *      required: true
 *      type: string
 *      format: estado
 *    - name: cliente
 *      description: cliente del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
    const nuevopedidos = req.body;
    //nuevopedidos.id = getRandomInt(1, 10000);
   // Era para testear que funcionara. console.log(`nuevopedidos ${JSON.stringify(nuevopedidos)}`);
    pedidos.push(nuevopedidos);
    res.json({msj: `pedido agregado correctamente`});
});

/**
 * @swagger
 * /pedidos/{id}:
 *  delete:
 *    description: elimina un pedido de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
    const id_pedidos= parseInt(req.params.id) ;
    let posicion = pedidos.findIndex(({id}) => id == id_pedidos);
     // quito el pedido del array
    pedidos.splice(posicion, 1);
    res.json({msj: `pedido eliminado correctamente`});
});

/**
 * @swagger
 * /pedidos:
 *  put:
 *    description: actualiza un pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: pedidos
 *      description: pedidos del cliente
 *      in: formData
 *      required: true
 *      type: string
 *    - name: estado
 *      description: estado del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    - name: cliente
 *      description: cliente del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
    id_pedidos = req.body.id;
    const indicepedidos = pedidos.findIndex(x => x.id == id_pedidos);

    const object = {
        id: parseInt(req.body.id),
        pedidos: req.body.pedidos,
        estado: req.body.estado,
        cliente: req.body.cliente
};
    pedidos[indicepedidos] = object;

    res.json({msj: `pedido actualizado correctamente`});
});




router.get('/', (req, res) => {
    res.json(pedidos);
});


module.exports = router;