const express = require('express');
const router = express.Router();
let productos = require('../models/productos');
let midUser = require('../middleware/usuarios');


/**
 * @swagger
 * /productos/:
 *  get:
 *    description: lista todos los productos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/',midUser.validar_cliente, (req, res) => {
  res.json(productos);
});

/**
 * @swagger
 * /productos:
 *  post:
 *    description: crea un producto
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: Productos
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', midUser.validar_admin, (req, res) => {
    const nuevoProductos = req.body;
    //nuevoProductos.id = req.body;  //getRandomInt(1, 10000);
    //console.log(`nuevoProductos ${JSON.stringify(nuevoProductos)}`);
    productos.push(nuevoProductos);

  res.json({msj: `producto agregado correctamente`});
});

/**
 * @swagger
 * /productos/{id}:
 *  delete:
 *    description: elimina un producto de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', midUser.validar_admin, (req, res) => {
  const id_productos= parseInt(req.params.id) ;
  let posicion = productos.findIndex(({id}) => id == id_productos);
   // quito el producto del array
  productos.splice(posicion, 1);
  res.json({msj: `producto eliminado correctamente`});
});

/**
 * @swagger
 * /productos:
 *  put:
 *    description: actualiza un producto
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', midUser.validar_admin, (req, res) => {
  id_productos = req.body.id;
  const indiceproductos = productos.findIndex(x => x.id == id_productos);

  const object = {
    id: parseInt(req.body.id),
    nombre: req.body.nombre,
  };
  productos[indiceproductos] = object;

  res.json({msj: `producto actualizado correctamente`});
});




router.get('/', (req, res) => {
  res.json(productos);
});

module.exports = router;