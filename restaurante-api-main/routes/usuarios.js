const express = require('express');
const router = express.Router();
let varUser = require('../models/usuarios');
//let usuario_logueado = require('../models/usuarios');
const getRandomInt = require('../../../Soluciones Profe/aleatorio');
let midUser = require('../middleware/usuarios');

/**
 * @swagger
 * /usuarios/:
 *  get:
 *    description: lista todos los usuarios
 *    responses:
 *      200:
 *        description: Success
 */
  router.get('/', midUser.validar_admin,(req, res) => {
    res.json(varUser.usuarios);
});

/**
 * @swagger
 * /usuarios:
 *  post:
 *    description: crea un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: usuarios
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    - name: contrasenia
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: type
 *      description: tipo del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
  router.post('/', (req, res) => {
    const nuevoUsuarios = req.body;
    nuevoUsuarios.id = getRandomInt(1, 10000);
   // Era para testear que funcionara. console.log(`nuevoUsuarios ${JSON.stringify(nuevoUsuarios)}`);
   varUser.usuarios.push(nuevoUsuarios);
    res.json({msj: `usuario agregado correctamente`});
});


/**
 * @swagger
 * /usuarios/{id}:
 *  delete:
 *    description: elimina un usuario de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', midUser.validar_admin, (req, res) => {
  const id_usuarios= parseInt(req.params.id) ;
  let posicion = varUser.usuarios.findIndex(({id}) => id == id_usuarios);
   // quito el usuario del array
   varUser.usuarios.splice(posicion, 1);
  res.json({msj: `usuario eliminado correctamente`});
});

// verificar que no sea el mismo correo 
// foreach del array usuario mail == usuario.email si es correo "ya existe e"

/**
 * @swagger
 * /usuarios:
 *  put:
 *    description: actualiza un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: contrasenia
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    - name: type
 *      description: tipo del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', midUser.validar_cliente, (req, res) => {
  id_usuarios = req.body.id;
  const indiceusuarios = varUser.usuarios.findIndex(x => x.id == id_usuarios);

  const object = {
    id: parseInt(req.body.id),
    nombre: req.body.nombre,
    contrasenia: req.body.contrasenia,
    email: req.body.email
  };
  varUser.usuarios[indiceusuarios] = object;

  res.json({msj: `usuario actualizado correctamente`});
});




/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    description: Logea un usuario
 *    parameters:
 *    - name: username
 *      description: nombre del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */


const login = (username, password) => varUser.usuarios.find(user => user.usuarios === username && user.contrasenia === password);

router.post('/login', (req, res) => {
  const username = req.body.username;
  console.log(`username: ${username}`);
  const password = req.body.password;
  console.log(`password: ${password}`);
  console.log(login(username, password));
// es lo que esta adentro
  varUser.usuario_logueado = (login(username, password));
  res.json({msg: 'Logueado correctamente'});

});

/**
 * @swagger
 * /usuarios/login:
 *  get:
 *    description: lista el usuario logueado
 *    responses:
 *      200:
 *        description: Success
 */

router.get('/login', (req, res) => {
  res.json(varUser.usuario_logueado);
});

/**
 * @swagger
 * /usuarios/logout:
 *  get:
 *    description: lista el usuario logueado
 *    responses:
 *      200:
 *        description: Success
 */

router.get('/logout', (req, res) => {
  varUser.usuario_logueado = {} 
  res.json(`usuario deslogeado correctamente`);
});




module.exports = router;

